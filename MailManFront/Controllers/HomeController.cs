﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Typesafe.Mailgun;

namespace MailManFront.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Send()
        {
            var key = ConfigurationManager.AppSettings["MAILGUN_API_KEY"];
            var smtpServer = ConfigurationManager.AppSettings["MAILGUN_SMTP_SERVER"];
            var client = new MailgunClient(smtpServer, key);
            client.SendMail(new System.Net.Mail.MailMessage("eyesasc@gmail.com", "vishnukool@gmail.com")
            {
                Subject = "Hello from mailgun",
                Body = "this is a test message from mailgun."
            });

            return View();
        }

    }
}
